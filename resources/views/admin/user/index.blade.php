@extends('admin.layouts.app')
@section('title', 'user')
@section('content')
    <div class="inner-block">
        <div class="pro-head">
            <h2>{{trans('messages.user_lable')}}
                <a href="{{route('user.add.form')}}" class="pull-right btn btn-primary">{{trans('messages.user_add_lable')}}</a>
            </h2>
        </div>
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>{{trans('messages.stt_lable')}}</th>
                <th>{{trans('messages.first_name_last_name_lable')}}</th>
                <th>{{trans('messages.email_lable')}}</th>
                <th>Loại tài khoản </th>
                <th>{{trans('messages.updated_at_lable')}}</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1;?>
            @forelse ($users as $user)
                <tr>
                    <td style="padding-bottom: 20px;">{{$i}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                        @if($user->type === \App\User::ADMIN)
                            Admin
                            @elseif($user->type === \App\User::EMPLOYEES)
                            Nhân viên
                            @else
                            Người dùng
                        @endif
                    </td>
                    <td>{{$user->updated_at}}</td>
                    <td style="position: relative;">
                         @if ($user->id != \Auth::user()->id && \Laratrust::hasRole('admin'))
                            <form action="{{route('user.delete',['id'=>$user->id])}}" class="form-edit formConfirmDeleteCommon" method="post" style="position: absolute; top : 3px; display: inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <input type="submit" class="btn btn-danger submitDelete" data-toggle="modal" data-target="#myModalDelete" value="Xóa">
                            </form>
                        @endif
                    </td>
                </tr>
                <?php $i++ ?>
            @empty
                <tr>
                    <td colspan="12"><h2 class="text-center no_data">{{ trans('messages.no_data') }}</h2></td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <div class="col-xs-12 text-center">
            {{ $users->links() }}
        </div>
        <div class="clearfix"></div>
    </div>
    <div id="myModalDelete" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Xóa tài khoản</h4>
                </div>
                <div class="modal-body">
                    <p>Bạn có chắc chắn muốn xóa tài khoản ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="btnConfirmDeleteCommon" >Đồng ý</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                </div>
            </div>

        </div>
    </div>
@endsection