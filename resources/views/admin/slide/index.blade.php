@extends('admin.layouts.app')
@section('title', 'Slide')
@section('content')
    <div class="inner-block">
        <h2>Hình ảnh slide
            <a href="{{route('slide.add.form')}}" class="pull-right btn btn-primary">Thêm hình ảnh</a>
        </h2>
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>{{trans('messages.stt_lable')}}</th>
                <th>{{trans('messages.image')}}</th>
            </tr>
            </thead>
            <tbody>
            @php
                $i = 1;
            @endphp
            @forelse($slides as $slide)
                <tr>
                    <td>{{$i}}</td>
                    <td><img src="{{asset('storage/slide/'.$slide->image)}}" alt="" class="img-responsive" style="max-width: 200px;"></td>
                </tr>
                @php
                    $i++;
                @endphp
            @empty
                <tr>
                    <td colspan="12"><h2 class="text-center no_data">{{ trans('messages.no_data') }}</h2></td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <!-- pagination -->
        <div class="row text-center">
            {{ $slides->links() }}
        </div>
    </div>
@endsection